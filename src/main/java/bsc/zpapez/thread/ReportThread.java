package bsc.zpapez.thread;

import bsc.zpapez.model.AccountManager;

public class ReportThread extends Thread {

	private static final long PERIOD = 60 * 1000;
	private AccountManager accountManager = new AccountManager();
	
	public ReportThread(AccountManager accountManager) {
		this.accountManager = accountManager;
	}
	
	@Override
	public void run() {
		while (true) {
			try {
				Thread.sleep(PERIOD);
				accountManager.printReport();
			} catch (InterruptedException e) {
				break;
			}
		}
	}

}
