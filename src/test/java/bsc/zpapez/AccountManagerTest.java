package bsc.zpapez;

import bsc.zpapez.exception.TransactionRefusedException;
import bsc.zpapez.model.AccountManager;
import bsc.zpapez.model.Transaction;
import junit.framework.TestCase;

public class AccountManagerTest extends TestCase {

	private AccountManager accountManager;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		accountManager = new AccountManager();
	}
	
	/**
	 * Refuse transaction when causing overflow
	 */
	public void testAddTransactionMaxInteger() {
		try {
			accountManager.addTransaction(new Transaction("AAA", Integer.MAX_VALUE - 1));
			accountManager.addTransaction(new Transaction("AAA", 123));
			fail();
		} catch (TransactionRefusedException e) {
			assertEquals(Integer.MAX_VALUE - 1, accountManager.getAccountBalance("AAA").intValue());
		}
	}
	
	/**
	 * Refuse transaction when causing underflow 
	 */
	public void testAddTransactionMinInteger() {
		try {
			accountManager.addTransaction(new Transaction("AAA", Integer.MIN_VALUE + 1));
			accountManager.addTransaction(new Transaction("AAA", -123));
			fail();
		} catch (TransactionRefusedException e) {
			assertEquals(Integer.MIN_VALUE + 1, accountManager.getAccountBalance("AAA").intValue());
		}
	}
}
