package bsc.zpapez.io;

import bsc.zpapez.exception.CommandException;
import bsc.zpapez.exception.QuitException;
import bsc.zpapez.model.Transaction;

public class InputParser {

	private static final String SEPARATOR = " ";
	private static final String QUIT = "quit";
	
	/**
	 * Read transaction with currency and amount parsed from the input 
	 * @param consoleLine input line
	 * @return transaction with currency and amount recorded
	 * @throws CommandException if input is not recognized as transaction
	 */
	public static Transaction readInput(String consoleLine) throws CommandException {
		if (consoleLine == null) {
			throw new CommandException("empty command");
		}
		
		if (consoleLine.equals(QUIT)) {
			throw new QuitException();
		}
		
		String[] fields = consoleLine.split(SEPARATOR);
		if (fields.length != 2) {
			throw new CommandException("unknown command");
		}
		String currency = readCurrency(fields[0]);
		Integer amount = readAmount(fields[1]);
		
		return new Transaction(currency, amount);
	}

	private static Integer readAmount(String amount) throws CommandException {
		try {
			return Integer.parseInt(amount);
		} catch (NumberFormatException e) {
			throw new CommandException("wrong number format: " + e.getMessage());
		}
	}

	private static String readCurrency(String currency) throws CommandException {
		if (currency.matches("[A-Z]{3}")) {
			return currency;
		} else {
			throw new CommandException("wrong currency code");
		}
	}

}
