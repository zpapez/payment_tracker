Payment Tracker
-------------------

Build
 - mvn package

Run
  - execute the JAR file from target folder
  - optional parameters with relative path of input files
  - e.g.: java -jar zpapez-1.0 file1.txt file2.txt

Assumptions
 - currency codes are 3 capital letters
 - amounts are interger values
 