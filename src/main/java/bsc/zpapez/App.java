package bsc.zpapez;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import bsc.zpapez.exception.CommandException;
import bsc.zpapez.exception.QuitException;
import bsc.zpapez.exception.TransactionRefusedException;
import bsc.zpapez.io.FileProcessor;
import bsc.zpapez.io.InputParser;
import bsc.zpapez.model.AccountManager;
import bsc.zpapez.model.Transaction;
import bsc.zpapez.thread.ReportThread;

/**
 *
 */
public class App {

	private AccountManager accountManager = new AccountManager();
	
	public static void main(String[] args) {
		App app = new App();
		
		if (args.length > 0) {
			app.processInputFiles(args);
		}
		
		Thread reporter = app.startOutputThread();
		
		app.startConsole();
		
		reporter.interrupt();
		try {
			reporter.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void processInputFiles(String[] args) {
		FileProcessor fileReader = new FileProcessor(accountManager);
		for (String path : args) {
			System.out.println("Processing file: " + path);
			try {
				fileReader.processTransactionsFromFile(path);
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}
	}

	private Thread startOutputThread() {
		Thread reportThread = new ReportThread(accountManager);
		reportThread.start();
		return reportThread;
	}

	private void startConsole() {
		System.out.println("Enter transaction (e.g. \"USD 122\") or \"quit\" to exit:");
		
		BufferedReader bufferedRead = new BufferedReader(new InputStreamReader(System.in));
		
		while (true) {
			try {
				String consoleLine = bufferedRead.readLine();
				Transaction transaction = InputParser.readInput(consoleLine);
				accountManager.addTransaction(transaction);
			} catch (IOException e) {
				System.out.println(e.getMessage());
			} catch (QuitException e) {
				break;
			} catch (CommandException | TransactionRefusedException e) {
				System.out.println(e.getMessage());
			}
		}
			
		try {
			bufferedRead.close();
		} catch (IOException e) {
				System.out.println(e.getMessage());
		}				
	}
}
