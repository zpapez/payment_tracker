package bsc.zpapez;

import bsc.zpapez.exception.CommandException;
import bsc.zpapez.exception.QuitException;
import bsc.zpapez.io.InputParser;
import junit.framework.TestCase;

public class InputParserTest extends TestCase {


	public void testReadInputNullLineThrowsCommandException() {
		try {
			InputParser.readInput(null);
			fail();
		} catch (CommandException e) {
		}
	}
	
	public void testReadInputEmptyLineThrowsCommandException() {
		try {
			InputParser.readInput("");
			fail();
		} catch (CommandException e) {
		}
	}
	
	public void testReadInputUknownCommandThrowsCommandException() {
		try {
			InputParser.readInput("xxx 123");
			fail();
		} catch (CommandException e) {
		}
	}
	
	public void testReadInputDecimalPointThrowsCommandException() {
		try {
			InputParser.readInput("AAA 12.34");
			fail();
		} catch (CommandException e) {
		}
	}
	
	public void testReadInputSuccessfulPositiveTransaction() {
		try {
			InputParser.readInput("AAA 123");
		} catch (CommandException e) {
			fail();
		}
	}
	
	public void testReadInputSuccessfulNegativeTransaction() {
		try {
			InputParser.readInput("AAA -123");
		} catch (CommandException e) {
			fail();
		}
	}
	
	public void testReadInputLongCommandThrowsCommandException() {
		try {
			InputParser.readInput("AAA 123 XXX");
			fail();
		} catch (CommandException e) {
		}
	}
	
	public void testReadInputQuit() {
		try {
			InputParser.readInput("quit");
			fail();
		} catch (QuitException e) {
			
		} catch (CommandException e) {
			fail();
		}
		
	}
}
