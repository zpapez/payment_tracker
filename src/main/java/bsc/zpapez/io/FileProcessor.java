package bsc.zpapez.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import bsc.zpapez.exception.CommandException;
import bsc.zpapez.exception.TransactionRefusedException;
import bsc.zpapez.model.AccountManager;
import bsc.zpapez.model.Transaction;

/**
 * 
 * @author zpapez
 *
 */
public class FileProcessor {
	
	private AccountManager accountManager = new AccountManager();

	public FileProcessor(AccountManager accountManager) {
		this.accountManager = accountManager;
	}

	/**
	 * Reads transactions from the specified file and sends them to the account manager
	 * @param path of the file
	 * @throws IOException
	 */
	public void processTransactionsFromFile(String path) throws IOException {
		if (path == null) {
			throw new IllegalArgumentException("path cannot be null");
		}
		File file = new File(path);
		String s = file.getCanonicalPath();
		if (file.exists() && !file.isDirectory()) {
			BufferedReader bufferRead = new BufferedReader(new FileReader(file));
			try {
				String line;
				while ((line = bufferRead.readLine()) != null) {
					try {
						Transaction transaction = InputParser.readInput(line);
						accountManager.addTransaction(transaction);
					} catch (CommandException | TransactionRefusedException e) {
						System.out.println(e.getMessage() + ": " + line);
					}
				}
			} finally {
				bufferRead.close();
			}
		} else {
			throw new IOException("file not found:" + path);
		}
		
	}

}
