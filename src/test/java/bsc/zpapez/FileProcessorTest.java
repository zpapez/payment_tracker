package bsc.zpapez;

import java.io.IOException;

import bsc.zpapez.io.FileProcessor;
import bsc.zpapez.model.AccountManager;
import junit.framework.TestCase;

public class FileProcessorTest extends TestCase {

	private FileProcessor fileProcessor;
	private AccountManager accountManager;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		accountManager = new AccountManager();
		fileProcessor = new FileProcessor(accountManager);
	}
	

	public void testProcessTransactionsFromFileNullThrowsIllegalArgumentException() {
		try {
			fileProcessor.processTransactionsFromFile(null);
		} catch (IllegalArgumentException e) {
			return;
		} catch (IOException e) {
			
		}
		fail();
	}
	
	public void testProcessTransactionsFromFileUknownThowsException() {
		try {
			fileProcessor.processTransactionsFromFile("xxx.zzz");
			fail();
		} catch (IOException e) {
		}
	}
	
	/**
	 * Empty file does not create any accounts
	 */
	public void testProcessTransactionsFromFileEmptyFile() {
		try {
			fileProcessor.processTransactionsFromFile("src/test/resources/empty.txt");
			assertEquals(0, accountManager.getNumberOfAccounts());
		} catch (IOException e) {
			fail(e.getMessage());
		}
	}
	
	/**
	 * Test 1 transaction for each account
	 */
	public void testProcessTransactionsFromFileTestFileWith3Accounts() {
		try {
			fileProcessor.processTransactionsFromFile("src/test/resources/test_3_accounts.txt");
			assertEquals(1, accountManager.getAccountBalance("AAA").intValue());
			assertEquals(2, accountManager.getAccountBalance("BBB").intValue());
			assertEquals(3, accountManager.getAccountBalance("CCC").intValue());
		} catch (IOException e) {
			fail();
		}
	}
	
	
	/**
	 * File with unknown commads does not create any accounts
	 */
	public void testProcessTransactionsFromFileUnknownCommands() {
		try {
			fileProcessor.processTransactionsFromFile("src/test/resources/uknown_commands.txt");
			assertEquals(0, accountManager.getNumberOfAccounts());
		} catch (IOException e) {
			fail(e.getMessage());
		}
	}
	
	/**
	 * Test with file that contains several transactions for each account
	 */
	public void testProcessTransactionsFromFileTestFileWithMultipleTransaction() {
		try {
			fileProcessor.processTransactionsFromFile("src/test/resources/test_multiple_transaction.txt");
			assertEquals(1, accountManager.getAccountBalance("AAA").intValue());
			assertEquals(4, accountManager.getAccountBalance("BBB").intValue());
			assertEquals(3, accountManager.getAccountBalance("CCC").intValue());
		} catch (IOException e) {
			fail();
		}
	}
	
}
