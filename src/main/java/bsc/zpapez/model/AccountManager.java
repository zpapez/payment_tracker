package bsc.zpapez.model;

import java.util.Hashtable;

import bsc.zpapez.exception.TransactionRefusedException;

/**
 * Holds accounts for several currencies and tracks their balance
 * @author zpapez
 *
 */
public class AccountManager {

	private Hashtable<String, Integer> accounts = new Hashtable<String, Integer>();

	public AccountManager() {
		
	}
	
	/**
	 * Adds transaction to the proper account
	 * - account balance is changed according to the amount specified in transaction
	 * @param transaction
	 * @throws TransactionRefusedException when account balance is too low or too high to record the transaction
	 */
	public synchronized void addTransaction(Transaction transaction) throws TransactionRefusedException {
		Integer balance = accounts.get(transaction.getCurrency());
		if (balance == null) {
			accounts.put(transaction.getCurrency(), transaction.getAmount());
		} else {
			if (Math.signum(transaction.getAmount()) * Math.signum(balance) > 0
					&& Math.abs(transaction.getAmount()) > Integer.MAX_VALUE - Math.abs(balance)) {
				throw new TransactionRefusedException();
			}
			accounts.put(transaction.getCurrency(), balance + transaction.getAmount());
		}
	}
	
	/**
	 * Prints the report of balance for each account where balance is not zero
	 */
	public synchronized void printReport() {
		System.out.println("ACOUNT BALANCE");
		for (String currency : accounts.keySet()) {
			if (accounts.get(currency) != 0) {
				System.out.println(currency + " " + accounts.get(currency));
			}
		}
	}
	
	/**
	 * 
	 * @param currency
	 * @return balance of the account for the currency specified
	 */
	public Integer getAccountBalance(String currency) {
		return accounts.get(currency);
	}

	/**
	 * 
	 * @return number of existing accounts
	 */
	public Object getNumberOfAccounts() {
		return accounts.keySet().size();
	}
}
